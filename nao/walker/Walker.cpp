/* 
 * File:   Walker.cpp
 * Author: halecivo
 * 
 * Created on 2. duben 2014, 18:34
 */

#include "Walker.h"
#include "classes/NaoCommand/NaoCommand.h"
#include "classes/NaoCommand/NaoCommandLookAround.h"
#include "classes/NaoCommand/NaoCommandSay.h"
#include "classes/NaoCommand/NaoCommandGoToTarget.h"
#include "classes/NaoCommand/NaoCommandPosture.h"
#include <alproxies/almotionproxy.h>
#include <althread/alcriticalsection.h>
#include <alproxies/allandmarkdetectionproxy.h>
#include <alproxies/alvideodeviceproxy.h>
#include <qi/log.hpp>
#include <limits.h> //INT_MAX
#include <iostream>

using namespace AL;

Walker::Walker(boost::shared_ptr<AL::ALBroker> pBroker, const std::string& pName) :
ALModule(pBroker, pName),
naomarkMutex(AL::ALMutex::createALMutex()) {
	setModuleDescription("Nao navigates on floor, delivering messages between rooms, greeting known people.");

	functionName("onNaoMark", getName(), "Method called when naomark is detected.");
	BIND_METHOD(Walker::onNaoMark);

	functionName("onObstacle", getName(), "Method called when movement stopped by obstacle.");
	BIND_METHOD(Walker::onObstacle);

	functionName("onHeadPressed", getName(), "Method called when head pressed.");
	BIND_METHOD(Walker::onHeadPressed);

	functionName("onBackHead", getName(), "Method called when back head button pressed.");
	BIND_METHOD(Walker::onBackHead);

	functionName("onFrontHead", getName(), "Method called when front head button pressed.");
	BIND_METHOD(Walker::onFrontHead);
}

Walker::~Walker() {
}

void Walker::straightWalk(double meters) {
	navigation.moveTo(meters, 0, 0);
}

void Walker::setAngle(double angle) {
	double difangle = angle - this->pos.orientation;
	motion.walkTo(0, 0, difangle);
	pos.orientation = angle;
}

void Walker::init() {
	tts = ALTextToSpeechProxy(getParentBroker());
	memoryProxy = ALMemoryProxy(getParentBroker());
	motion = ALMotionProxy(getParentBroker());
	navigation = ALNavigationProxy(getParentBroker());
	compass = ALVisualCompassProxy(getParentBroker());
        ALVideoDeviceProxy video = ALVideoDeviceProxy(getParentBroker());
        
        // use top camera
        video.setActiveCamera(0);

	positionDatabase = NaoMarkPositionDatabase();

	ALLandMarkDetectionProxy landmark = ALLandMarkDetectionProxy(getParentBroker());

	int periodInMiliseconds = 1000;
	landmark.subscribe("Test_LandMark", periodInMiliseconds, 0.0);
        
	tts.setLanguage("Czech");

	navigation.setSecurityDistance(0.35);
	compass.enableReferenceRefresh(false);
	compass.setResolution(1);

	posture = ALRobotPostureProxy(getParentBroker());


	memoryProxy.subscribeToEvent("DangerousObstacleDetected", "Walker", "onObstacle");
	memoryProxy.subscribeToEvent("MiddleTactilTouched", "Walker", "onHeadPressed");
	memoryProxy.subscribeToEvent("FrontTactilTouched", "Walker", "onFrontHead");
	memoryProxy.subscribeToEvent("RearTactilTouched", "Walker", "onBackHead");
        memoryProxy.subscribeToEvent("LandmarkDetected", "Walker", "onNaoMark");

	position = NaoInternalPosition();

	//compass.subscribe("VisualCompassTest");

	//tts.say(wz);
	//tts.say(wy);
	//run();
}

void Walker::lookAround() {
	NaoCommand * lookaround = new NaoCommandLookAround(&motion);
	//commands.push(lookaround);
	lookaround->run();
}

bool Walker::moveToward(float x, float y, float theta) {
	navigation.moveTo(x, y, theta);

	position.move(x, y);
	return false;
}

void Walker::onNaoMark() {
	AL::ALCriticalSection section(naomarkMutex);
	ALValue mark = memoryProxy.getData("LandmarkDetected");
	memoryProxy.unsubscribeToEvent("LandmarkDetected", "Walker");
	if (mark.getSize() < 2) {
		memoryProxy.subscribeToEvent("LandmarkDetected", "Walker", "onNaoMark");
		section.forceUnlockNow();
		return;
	} else {
		int detectedPos = mark[1][0][1][0];
		//qiLogInfo("Walker") << "Found landmark id = " << detectedPos << std::endl;
		if (this->marksSeen.count(detectedPos)) {
			if (this->marksSeen.at(detectedPos)) {
				memoryProxy.subscribeToEvent("LandmarkDetected", "Walker", "onNaoMark");
				section.forceUnlockNow();
				return;
			} else {
				this->marksSeen.at(detectedPos) = true;
			}
		} else {
			this->marksSeen.insert(std::make_pair(detectedPos, true));
		}
		try {
			NaoPosition tmp_pos = NaoPosition(mark, getParentBroker());
			updateNaoPosition(tmp_pos.getNaoMarkId(), tmp_pos.getNaoMarkDistance(), tmp_pos.getNaoMarkAngle(), tmp_pos.getNaoAngle());
			sayNaoPositionShortVersion();
		} catch (const char * exp) {
			//tts.say(exp);
			qiLogInfo("module.example") << exp << std::endl;
		}
	}
	memoryProxy.subscribeToEvent("LandmarkDetected", "Walker", "onNaoMark");
	section.forceUnlockNow();

}

bool Walker::updateNaoPosition(int naoMarkId, double naoMarkDistance, double naoMarkAngleInGrades, double naoAngleInGrades) {
	NaoMarkPositionDatabase db;
	s_naoMarkPosition naoMarkPosition = db.getNaoMarkPositionById(naoMarkId);
	if (!strcmp(naoMarkPosition.room.building, "INVALID")) {
		char sayBuf[150];
		sprintf(sayBuf, "Značku %d neznám", naoMarkPosition.mark.id);
		tts.say(sayBuf);
		return false;
	}
	naoMarkAngleInGrades = (int) (naoMarkAngleInGrades + naoMarkPosition.mark.angle) % 360;
        
	qiLogInfo("Walker") << "Distance = " << naoMarkDistance << " X = " << naoMarkPosition.mark.coord_X << " B = " << naoMarkPosition.room.building << std::endl;
	if ((naoMarkAngleInGrades < 0) || (naoMarkAngleInGrades >= 360)) {
		//		char sayBuf[150];
		//		sprintf(sayBuf, "Nesmyslný úhel %d stupňů", naoMarkAngleInGrades);
		//		tts.say(sayBuf);
		return false;
	}

	if (naoMarkAngleInGrades <= 90) {
		double angleInRadians = (naoMarkAngleInGrades / 180) * M_PI;
		pos.X_coord = (naoMarkPosition.mark.coord_X - sin(angleInRadians) * naoMarkDistance * 10) + 0.5;
		pos.Y_coord = (naoMarkPosition.mark.coord_Y - cos(angleInRadians) * naoMarkDistance * 10) + 0.5;
	} else if (naoMarkAngleInGrades < 180) {
		double angleInRadians = ((naoMarkAngleInGrades - 90) / 180) * M_PI;
		pos.X_coord = (naoMarkPosition.mark.coord_X - sin(angleInRadians) * naoMarkDistance * 10) + 0.5;
		pos.Y_coord = (naoMarkPosition.mark.coord_Y + cos(angleInRadians) * naoMarkDistance * 10) + 0.5;
	} else if (naoMarkAngleInGrades <= 270) {
		double angleInRadians = ((naoMarkAngleInGrades - 180) / 180) * M_PI;
		pos.X_coord = (naoMarkPosition.mark.coord_X + sin(angleInRadians) * naoMarkDistance * 10) + 0.5;
		pos.Y_coord = (naoMarkPosition.mark.coord_Y + cos(angleInRadians) * naoMarkDistance * 10) + 0.5;
	} else if (naoMarkAngleInGrades < 360) {
		double angleInRadians = ((naoMarkAngleInGrades - 270) / 180) * M_PI;
		pos.X_coord = (naoMarkPosition.mark.coord_X + sin(angleInRadians) * naoMarkDistance * 10) + 0.5;
		pos.Y_coord = (naoMarkPosition.mark.coord_Y - cos(angleInRadians) * naoMarkDistance * 10) + 0.5;
	}
	pos.orientation = ((int) (naoMarkPosition.mark.angle - naoAngleInGrades + 360 + 0.5)) % 360;
	memcpy(&pos.room, &naoMarkPosition.room, sizeof (s_room));

	return true;
}

s_subTargetPosition* Walker::getDirections(const s_targetPosition* targetPos) {
	int nrOfsubTargets = 4;
	s_subTargetPosition *ret = new s_subTargetPosition[nrOfsubTargets];
	for (int i = 0; i < nrOfsubTargets; i++) {
		ret[i].subId = i;
		ret[i].numberOfTargets = nrOfsubTargets;
	}

	// goto y axis -> x=0
	if (pos.X_coord > 0) {
		ret[0].radiusInDeg = 270;

	} else {
		ret[0].radiusInDeg = 90;
	}
	ret[0].distanceToGoInCm = abs(pos.X_coord);
	ret[0].x = 0;
	ret[0].y = pos.Y_coord;

	// go to target y 
	if (targetPos->y > pos.Y_coord) {
		ret[1].radiusInDeg = 0;
	} else {
		ret[1].radiusInDeg = 180;
	}
	ret[1].distanceToGoInCm = abs(pos.Y_coord - targetPos->y);
	ret[1].x = 0;
	ret[1].y = targetPos->y;

	//goto target x
	if (targetPos->x > 0) {
		ret[2].radiusInDeg = 90;
	} else {
		ret[2].radiusInDeg = 270;
	}
	ret[2].distanceToGoInCm = abs(targetPos->x);
	ret[2].x = targetPos->x;
	ret[2].y = targetPos->y;

	// rotate to final position
	ret[3].radiusInDeg = targetPos->radiusInDeg;
	ret[3].distanceToGoInCm = 0;
	ret[3].x = targetPos->x;
	ret[3].y = targetPos->y;
	return ret;
}

bool Walker::gotoDestinationInBuildingA_ById(int targetId) {
	std::string building = "TH:A";
	s_targetPosition target = db.getTargetPositionById(building.c_str(), targetId);
	if (target.x == INT_MAX) {
		throw "Unkown target id, no move done.";
	}
	if (target.x > 10000) {
		throw "Probably wrong target id, no move done.";
	}
	NaoCommand * gototarget = new NaoCommandGoToTarget(&motion, &target, &pos);
	//commands.push(gototarget);
	gototarget->run();
	return true; //gotoDestination(&target);
}

bool Walker::gotoDestination(const s_targetPosition* targetPos) {
	s_subTargetPosition * subTargets = getDirections(targetPos);
	for (int i = 0; i < subTargets[0].numberOfTargets; i++) {
		int rotateInDeg = subTargets[i].radiusInDeg;
		int distanceToGoInCm = subTargets[i].distanceToGoInCm;
		setAngle(rotateInDeg);
		straightWalk((double) distanceToGoInCm / 10.0);
		pos.X_coord = subTargets[i].x;
		pos.Y_coord = subTargets[i].y;
		// after this rotation and walking, Nao should stand at pos:
		//subTargets[i].x and subTargets[i].y;
	}
	delete subTargets;
}

void Walker::sayNaoPosition() {
	char sayBuf[200];
	sprintf(sayBuf, "Jsem v budově %s, v patře %d, v místnosti %s , na souřadnicích %d a %d. Moje otočení je %d stupňů.", pos.room.building, pos.room.building_floor, pos.room.name, pos.X_coord, pos.Y_coord, pos.orientation);
	NaoCommand * say = new NaoCommandSay(&tts, sayBuf);
	//commands.push(say);
	say->run();
}

void Walker::sayNaoPositionShortVersion() {
	char sayBuf[200];
	if (pos.X_coord == INT_MAX) {
		sprintf(sayBuf, "Nevím kde jsem.");
	} else if (pos.X_coord > 10000) {
		sprintf(sayBuf, "Nevím kde jsem.");
		qiLogInfo("Walker") << "wtf" << std::endl;
	} else {
		sprintf(sayBuf, "Jsem na souřadnicích %d a %d, otočení %d stupňů", pos.X_coord, pos.Y_coord, pos.orientation);
	}
	NaoCommand * say = new NaoCommandSay(&tts, sayBuf);
	//commands.push(say);
	say->run();
}

void Walker::onObstacle() {
	this->tts.say("Zastavila mě překážka.");
}

void Walker::updatePosition() {
	//NaoPosition pos = NaoPosition()
}

void Walker::clearMarksSeen() {
	qiLogInfo("Walker") << "Marks seen cleared." << std::endl;
	for (std::map<int, bool>::iterator it = this->marksSeen.begin(); it != this->marksSeen.end(); ++it) {
		it->second = false;
	}
}

void Walker::onHeadPressed() {
	//this->clearMarksSeen();
        float state = memoryProxy.getData("MiddleTactilTouched");
	if(state > 0.5) return;
	this->gotoDestinationInBuildingA_ById(1);
}

void Walker::onFrontHead() {
	float state = memoryProxy.getData("FrontTactilTouched");
	if(state > 0.5) return;
	qiLogInfo("Walker") << "Front head touched, standing and looking around." << std::endl;
	NaoCommand * pose = new NaoCommandPosture(&posture, "Stand");
	//commands.push(pose);
	pose->run();
	NaoCommand * look = new NaoCommandLookAround(&this->motion);
	//commands.push(look);
	look->run();
}

void Walker::onBackHead() {
        float state = memoryProxy.getData("RearTactilTouched");
	if(state > 0.5) return;
	this->clearMarksSeen();
	qiLogInfo("Walker") << "Back head touched, sitting." << std::endl;
	NaoCommand * pose = new NaoCommandPosture(&posture, "Sit");
	pose->run();
	//commands.push(pose);
}

void Walker::visualCompassTest() {
	ALValue deviation, matchInfo;
	compass.getReferenceImage();

	tts.say("Vyfotil jsem si referenci.");

	sleep(2);

	compass.getCurrentImage();
	tts.say("Vyfotil jsem si nový obrázek.");

	deviation = memoryProxy.getData("VisualCompass/Deviation");
	matchInfo = memoryProxy.getData("VisualCompass/Match");

	float wy = deviation[0][0], wz = deviation[0][1];
	// Convert it to degrees.
	wz = wz * 180.0f / 3.14f;
	wy = wy * 180.0f / 3.14f;

	char sayBuf[100];
	sprintf(sayBuf, "otočení je %,2f stupňů a %,2f stupňů", wz, wy);
	tts.say(sayBuf);

}

void Walker::run() {
	NaoCommand * say = new NaoCommandSay(&tts, "Jsem připraven.");
	say->run();
	//commands.push(say);
	while (false) {
		if (commands.size() > 0) {
			NaoCommand * cmd = commands.front();
			cmd->run();
			qiLogInfo("Walker") << "Running a command." << std::endl;
			commands.pop();
		}
	}
}

