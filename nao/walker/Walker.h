/* 
 * File:   Walker.h
 * Author: halecivo
 *
 * Created on 2. duben 2014, 18:34
 */

#ifndef WALKER_H
#define	WALKER_H

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <alproxies/almemoryproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/alnavigationproxy.h>
#include <althread/almutex.h>
#include <alproxies/alrobotpostureproxy.h>
#include <alproxies/alvisualcompassproxy.h>
#include "classes/NaoMarkPositionDatabase.h"
#include "classes/NaoPosition.h"
#include "classes/NaoInternalPosition.h"
#include "classes/NaoCommand/NaoCommand.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <map>
#include <queue>


namespace AL {
	// This is a forward declaration of AL:ALBroker which
	// avoids including <alcommon/albroker.h> in this header
	class ALBroker;
}

class Walker : public AL::ALModule {
public:
	Walker(boost::shared_ptr<AL::ALBroker> pBroker, const std::string& pName);
	virtual ~Walker();
	virtual void init();

	// events
	void onNaoMark();
	void onObstacle();
	void onHeadPressed();
	void onFrontHead();
	void onBackHead();
	//void onFace();
	//void onSpeech();

	void lookAround();
	//void walkTo(NaoPosition position);
	/**
	 * @brief updates nao's internal position
	 * @details Nao stores his actual position, which is changed when walking.
	 * With a time its more and more inaccurate, therefore it's good to update
	 * it when possible. Nao should update position everytime he sees a mark.
	 * Nao ignores the mark until he moves without seeing it. Computing position
	 * from mark vision can be also inaccurate, therefore it might be good idea
	 * to update position to a weighed average of current position and position
	 * computed from mark. Nao should also ignore new position if it is very
	 * different from his internal position.
	 */
	void updatePosition();
	/**
	 * @brief moves toward specified direction
	 * @details Performs safe (stop before obstacle) movement with update of
	 * position.
	 * @param x distance in forward/backward direction
	 * @param y distance in lateral direction. Positive for left, negative for right.
	 * @param theta final rotation in radians. Positive means anticlockwise, negative clockwise.
	 * @retval true if move was completed
	 * @retval false if move was interrupted by obstacle
	 */
	bool moveToward(float x, float y, float theta);
	/**
	 * Update position of Nao if naoMark detected
	 * @param naoMarkId [in] Id of detected naoMark
	 * @param naoMarkDistance [in] Distance from naoMark in centimetres
	 * @param naoMarkAngle [in] Angle of naoMark in degrees according to Nao position (0 - 359 degrees, same as analog clocks: 0 degrees is 0 hours (in front of Nao) , 90 degrees are 3 hours (on the right side of Nao), 180 degrees are 6 hours (behind Nao), 270 degrees are 9 hours (on the left side of Nao))
	 * @param naoAngle [in] Angle of Nao in degrees from point [0,0] (0 - 359 degrees, same as analog clocks: 0 degrees is 0 hours (Y+ direction) , 90 degrees are 3 hours (X+ direction), 180 degrees are 6 hours (Y- direction), 270 degrees are 9 hours (X- direction))
	 * @retval True if update was successful
	 * @retval False if update failed (unknown naoMarkId, invalid distance or invalid angle)
	 */
	bool updateNaoPosition(int naoMarkId, double naoMarkDistance, double naoMarkAngle, double naoAngle = 0);
	/**
	 * Say where Nao is (building, floor, room, coordination [X,Y])
	 */
	void sayNaoPosition();
	/**
	 * Say where Nao is (just coordination [X,Y])
	 */
	void sayNaoPositionShortVersion();

	/**
	 * @brief Clear list of naoMarks processed.
	 * @details This should be called when Nao moves, so his relative position
	 * to NaoMarks is invalid.
	 */
	void clearMarksSeen();

	void visualCompassTest();
	void run();
	/**
	 * Very very basic navigation.
	 * @param targetPos [in] Position of target = end position
	 * @return Return (several) waypoints on way to targets. Each waypoint differs only in x or y coord from previous waypoint.
	 */
	s_subTargetPosition *getDirections(const s_targetPosition *targetPos);

	/**
	 * Move Nao to target
     * @param targetPos [in] position of Target
     * @retval true if Nao reach the target
     * @retval false if Nao didn´t reach the target
     */
	bool gotoDestination(const s_targetPosition *targetPos);
	
	/**
	 * Move Nao to target in building TH:A, the target is specified by Id 
     * @param id [in] Id of target in building TH:A
     * @return 
     */
	bool gotoDestinationInBuildingA_ById(int targetId);
        
        void setAngle(double angle);
        void straightWalk(double meters);
private:
	AL::ALMemoryProxy memoryProxy;
	AL::ALTextToSpeechProxy tts;
	AL::ALNavigationProxy navigation;
	AL::ALMotionProxy motion;
	AL::ALVisualCompassProxy compass;
        AL::ALRobotPostureProxy posture;
	NaoMarkPositionDatabase positionDatabase;
	boost::shared_ptr<AL::ALMutex> naomarkMutex;
	NaoMarkPositionDatabase db;
	s_position pos;
	NaoInternalPosition position;
	std::map<int, bool> marksSeen;
	std::queue <NaoCommand*> commands;
};

#endif	/* WALKER_H */

