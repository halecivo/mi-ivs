/* 
 * File:   NaoCommand.h
 * Author: halecivo
 *
 * Created on 10. květen 2014, 16:53
 */

#ifndef NAOCOMMAND_H
#define	NAOCOMMAND_H

#include <boost/shared_ptr.hpp>
#include <alproxies/altexttospeechproxy.h>
#include <alcommon/almodule.h>
#include <string>
#include "../../structs.h"
class NaoCommand {
public:
    NaoCommand();
    NaoCommand(const NaoCommand& orig);
    virtual ~NaoCommand();
    virtual void run() = 0;
private:
    boost::shared_ptr<AL::ALBroker> pBroker;
protected:
    std::string name;
};

#endif	/* NAOCOMMAND_H */

