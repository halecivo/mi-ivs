/* 
 * File:   NaoCommand.cpp
 * Author: halecivo
 * 
 * Created on 10. květen 2014, 16:53
 */

#include <string.h>

#include "NaoCommandGoToTarget.h"

using namespace AL;

NaoCommandGoToTarget::NaoCommandGoToTarget(ALMotionProxy * motion, s_targetPosition *p_target, s_position* p_naoPosition) {
	//this->name = "Go to Target";
	this->motion = motion;
	this->naoPosition = p_naoPosition;
	memcpy(&this->target, p_target, sizeof(s_targetPosition));
}

s_subTargetPosition* NaoCommandGoToTarget::getDirections() {
	int nrOfsubTargets = 4;
	s_subTargetPosition *ret = new s_subTargetPosition[nrOfsubTargets];
	for (int i = 0; i < nrOfsubTargets; i++) {
		ret[i].subId = i;
		ret[i].numberOfTargets = nrOfsubTargets;
	}

	// goto y axis -> x=0
	if (naoPosition->X_coord > 0) {
		ret[0].radiusInDeg = 270;

	} else {
		ret[0].radiusInDeg = 90;
	}
	ret[0].distanceToGoInCm = abs(naoPosition->X_coord);
	ret[0].x = 0;
	ret[0].y = naoPosition->Y_coord;

	// go to target y 
	if (target.y > naoPosition->Y_coord) {
		ret[1].radiusInDeg = 0;
	} else {
		ret[1].radiusInDeg = 180;
	}
	ret[1].distanceToGoInCm = abs(naoPosition->Y_coord - target.y);
	ret[1].x = 0;
	ret[1].y = target.y;

	//goto target x
	if (target.x > 0) {
		ret[2].radiusInDeg = 90;
	} else {
		ret[2].radiusInDeg = 270;
	}
	ret[2].distanceToGoInCm = abs(target.x);
	ret[2].x = target.x;
	ret[2].y = target.y;

	// rotate to final position
	ret[3].radiusInDeg = target.radiusInDeg;
	ret[3].distanceToGoInCm = 0;
	ret[3].x = target.x;
	ret[3].y = target.y;
	return ret;
}

NaoCommandGoToTarget::NaoCommandGoToTarget(const NaoCommandGoToTarget& orig) {
}

void NaoCommandGoToTarget::run() {
	gotoDestination();
}

bool NaoCommandGoToTarget::gotoDestination() {
	s_subTargetPosition * subTargets = getDirections();
	for (int i = 0; i < subTargets[0].numberOfTargets; i++) {
		int rotateInDeg = subTargets[i].radiusInDeg;
		int distanceToGoInDecimeters = subTargets[i].distanceToGoInCm;
		setAngle(rotateInDeg);
		straightWalk((double) distanceToGoInDecimeters / 10.0);
		naoPosition->X_coord = subTargets[i].x;
		naoPosition->Y_coord = subTargets[i].y;
		// after this rotation and walking, Nao should stand at pos:
		//subTargets[i].x and subTargets[i].y;
	}
	delete subTargets;
}

void NaoCommandGoToTarget::straightWalk(double meters) {
	motion->walkTo(meters, 0, 0);
}

void NaoCommandGoToTarget::setAngle(double angle) {
	double difangle = angle - this->naoPosition->orientation;
	motion->walkTo(0, 0, difangle);
	naoPosition->orientation = angle;
}

NaoCommandGoToTarget::~NaoCommandGoToTarget() {
}

