/* 
 * File:   NaoCommandGoToTarget.h
 * Author: halecivo
 *
 * Created on 14. květen 2014, 16:53
 */

#ifndef NAOCOMMANDGOTOTARGET_H
#define	NAOCOMMANDGOTOTARGET_H

#include <boost/shared_ptr.hpp>
#include "alproxies/almotionproxy.h"
#include <alcommon/almodule.h>
#include <string>
#include "NaoCommand.h"


class NaoCommandGoToTarget : public NaoCommand {
public:
	NaoCommandGoToTarget(AL::ALMotionProxy * motion, s_targetPosition *target, s_position *naoPosition);
	NaoCommandGoToTarget(const NaoCommandGoToTarget& orig);
	virtual ~NaoCommandGoToTarget();
	void run();
private:
	s_subTargetPosition* getDirections();
	bool gotoDestination();
	void setAngle(double angle);
	void straightWalk(double meters);
	AL::ALMotionProxy * motion;
	s_position *naoPosition;
	s_targetPosition target;
};

#endif	/* NAOCOMMANDGOTOTARGET_H */

