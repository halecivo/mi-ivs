/* 
 * File:   NaoCommandLookAround.cpp
 * Author: halecivo
 * 
 * Created on 10. květen 2014, 20:05
 */

#include "NaoCommandLookAround.h"
#include <alproxies/almotionproxy.h>

using namespace AL;

NaoCommandLookAround::NaoCommandLookAround(AL::ALMotionProxy * motion) {
    this->motion = motion;
    this->name = "Look around";
}

NaoCommandLookAround::NaoCommandLookAround(const NaoCommandLookAround& orig) {
}

NaoCommandLookAround::~NaoCommandLookAround() {
}

void NaoCommandLookAround::run(){
	motion->stiffnessInterpolation("HeadYaw", 1.0f, 1.0f);

	ALValue targetAngles = ALValue::array(-2.05f, 2.05f, 0.0f);
	ALValue targetTimes = ALValue::array(4.0f, 8.0f, 12.0f);

	motion->angleInterpolation("HeadYaw", targetAngles, targetTimes, true);

	motion->stiffnessInterpolation("HeadYaw", 0.0f, 1.0f);
}