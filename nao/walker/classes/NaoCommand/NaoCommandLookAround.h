/* 
 * File:   NaoCommandLookAround.h
 * Author: halecivo
 *
 * Created on 10. květen 2014, 20:05
 */

#ifndef NAOCOMMANDLOOKAROUND_H
#define	NAOCOMMANDLOOKAROUND_H

#include "NaoCommand.h"
#include "alproxies/almotionproxy.h"

class NaoCommandLookAround : public NaoCommand {
public:
    NaoCommandLookAround(AL::ALMotionProxy * motion);
    NaoCommandLookAround(const NaoCommandLookAround& orig);
    virtual ~NaoCommandLookAround();
    void run();
private:
    AL::ALMotionProxy * motion;
};

#endif	/* NAOCOMMANDLOOKAROUND_H */

