/* 
 * File:   NaoCommandPosture.cpp
 * Author: halecivo
 * 
 * Created on 10. květen 2014, 17:44
 */

#include "NaoCommandPosture.h"
#include <qi/log.hpp>
#include <iostream>

using namespace AL;

NaoCommandPosture::NaoCommandPosture(AL::ALRobotPostureProxy * posture, std::string finalPose) {
    this->name = "Posture";
    this->posture = posture;
    this->finalPose = finalPose;
}

NaoCommandPosture::NaoCommandPosture(const NaoCommandPosture& orig) {
}

NaoCommandPosture::~NaoCommandPosture() {
}

void NaoCommandPosture::run(){
    qiLogInfo("NaoCommandPosture") << "Posture: " << this->finalPose << std::endl;
    this->posture->goToPosture(this->finalPose,1.0);
}
