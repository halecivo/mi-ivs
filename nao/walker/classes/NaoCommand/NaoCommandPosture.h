/* 
 * File:   NaoCommandPosture.h
 * Author: halecivo
 *
 * Created on 10. květen 2014, 17:44
 */

#ifndef NAOCOMMANDPOSTURE_H
#define	NAOCOMMANDPOSTURE_H

#include "NaoCommand.h"
#include <alproxies/alrobotpostureproxy.h>

class NaoCommandPosture : public NaoCommand {
public:
    NaoCommandPosture(AL::ALRobotPostureProxy * posture, std::string finalPose);
    NaoCommandPosture(const NaoCommandPosture& orig);
    void run();
    virtual ~NaoCommandPosture();
private:
    std::string finalPose;
    AL::ALRobotPostureProxy * posture;
};

#endif	/* NAOCOMMANDSAY_H */

