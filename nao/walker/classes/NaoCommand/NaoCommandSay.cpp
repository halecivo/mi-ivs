/* 
 * File:   NaoCommandSay.cpp
 * Author: halecivo
 * 
 * Created on 10. květen 2014, 17:44
 */

#include "NaoCommandSay.h"
#include <qi/log.hpp>
#include <iostream>

using namespace AL;

NaoCommandSay::NaoCommandSay(AL::ALTextToSpeechProxy * tts, std::string phraseToSay) {
    this->name = "Say";
    this->tts = tts;
    this->phraseToSay = phraseToSay;
}

NaoCommandSay::NaoCommandSay(const NaoCommandSay& orig) {
}

NaoCommandSay::~NaoCommandSay() {
}

void NaoCommandSay::run(){
    qiLogInfo("NaoCommandSay") << "Saying: " << this->phraseToSay << std::endl;
    tts->say(this->phraseToSay);
}
