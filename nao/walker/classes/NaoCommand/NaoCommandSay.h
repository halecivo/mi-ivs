/* 
 * File:   NaoCommandSay.h
 * Author: halecivo
 *
 * Created on 10. květen 2014, 17:44
 */

#ifndef NAOCOMMANDSAY_H
#define	NAOCOMMANDSAY_H

#include "NaoCommand.h"
#include <alproxies/altexttospeechproxy.h>

class NaoCommandSay : public NaoCommand {
public:
    NaoCommandSay(AL::ALTextToSpeechProxy * tts, std::string phraseToSay);
    NaoCommandSay(const NaoCommandSay& orig);
    void run();
    virtual ~NaoCommandSay();
private:
    std::string phraseToSay;
    AL::ALTextToSpeechProxy * tts;
};

#endif	/* NAOCOMMANDSAY_H */

