/* 
 * File:   NaoInternalPosition.cpp
 * Author: halecivo
 * 
 * Created on 9. duben 2014, 9:55
 */

#include "NaoInternalPosition.h"
#include <math.h>

NaoInternalPosition::NaoInternalPosition() {
    x = 0.0;
    y = 0.0;
    z = 0;
    rad = 0.0;
}

NaoInternalPosition::~NaoInternalPosition() {
}

float NaoInternalPosition::Rad(){
    return rad;
}

float NaoInternalPosition::X(){
    return x;
}

float NaoInternalPosition::Y(){
    return y;
}

int NaoInternalPosition::Z(){
    return z;
}

void NaoInternalPosition::move(float x, float y){
    this->x = x * cos(this->rad) + y * sin(this->rad);
    this->y = x * sin(this->rad) + y * cos(this->rad);
}

void NaoInternalPosition::move(float x, float y, float rad){
    throw "Not Implemented!";
}

void NaoInternalPosition::setPosition(float x, float y, float rad){
    this->x = x;
    this->y = y;
    this->rad = rad;
}

void NaoInternalPosition::setZ(int z){
    this->z = z;
}