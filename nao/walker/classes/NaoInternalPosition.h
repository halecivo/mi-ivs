/* 
 * File:   NaoInternalPosition.h
 * Author: halecivo
 *
 * Created on 9. duben 2014, 9:55
 */

#ifndef NAOINTERNALPOSITION_H
#define	NAOINTERNALPOSITION_H

/**
 * @brief stores Nao's position
 * @details Position should be updated when Nao walks or
 * when he sees item with known position.
 */
class NaoInternalPosition {
public:
    NaoInternalPosition();
    virtual ~NaoInternalPosition();
    void move(float x, float y);
    void move(float x, float y, float rad);
    void setPosition(float x, float y, float rad);
    void setZ(int z);
    
    float X();
    float Y();
    float Rad();
    int Z();
private:
    float x;
    float y;
    int z;
    float rad;
};

#endif	/* NAOINTERNALPOSITION_H */

