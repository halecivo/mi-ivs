/* 
 * File:   NaoMarkPositionDatabase.cpp
 * Author: hajekm13
 * 
 * Created on 08 April 2014, 14:11
 */

#include "NaoMarkPositionDatabase.h"
#include <sstream> // stringstream
#include <stdio.h> // mempcpy
#include <string.h> //mempcy
#include <stdlib.h> //atoi
#include <limits.h> //INT_MAX
#include <qi/log.hpp>

#define tableName "naoMarks"
#define primaryKeyName "naoMark_id"

using namespace std;

/**
 * constructor = open database
 */
NaoMarkPositionDatabase::NaoMarkPositionDatabase() {
	if (!openDatabase()) {
		// say "I can't connect to NaoMark database"
		throw "cant open db\n";
	} else {
		//closeDatabase();
		; //db opened (in no db found, db is created!!)
	}
}

/**
 * Open database
 * @return true if database successfully opened
 * @return false if database cannot be opened (db not found / already opened)
 */
bool NaoMarkPositionDatabase::openDatabase() {
	return (!sqlite3_open("database/NaoMark.db", &database));
}

/**
 * Close database
 * @return true if database successfully closed
 * @return false if database cannot be closed (db not found / already closed) 
 */
bool NaoMarkPositionDatabase::closeDatabase() {
	return (sqlite3_close(database));
}

/**
 * Find NaoMark in db and return NaoMark´s position
 * @param NaoMark [in] Id of NaoMark 
 * @return The position of specified NaoMark
 */
s_naoMarkPosition NaoMarkPositionDatabase::getNaoMarkPositionById(int NaoMark) {
	s_naoMarkPosition ret; // return structure

	stringstream selectQuery;
	selectQuery << "SELECT * FROM " << tableName << " WHERE " << primaryKeyName << " == " << NaoMark << ";";
	qiLogInfo("sqlite3") << selectQuery.str().c_str() << std::endl;
	sqlite3_stmt *selectStatement;
	if ((sqlite3_prepare_v2(database, selectQuery.str().c_str(), -1, &selectStatement, 0)) == SQLITE_OK) {
		int cols = sqlite3_column_count(selectStatement);
		//cout << "returned columns: " << cols << endl;
		if (sqlite3_step(selectStatement) == SQLITE_ROW) {
			for (int i = 0; i < cols; i++) {
				if ((char*) sqlite3_column_text(selectStatement, i) != NULL) {
					fillOneItemInStructureNaoMarkPosition(&ret, sqlite3_column_name(selectStatement, i), sqlite3_column_text(selectStatement, i));
				}
			}
			sqlite3_finalize(selectStatement);
			return ret;
		} else { // no result from db = unknown naoMark
			sqlite3_finalize(selectStatement);
			setStructureNaoMarkAsInvalid(&ret);
			ret.mark.id = NaoMark;
			return ret;
		}
	} else {
		//	setStructureAsInvalid(&ret);
		throw "Wrong db name (or wrong select query)!\n";
		//		return ret;
	}
	return ret;
}

/**
 * Find Target in db and return its´s position
 * @param building [in] name of building
 * @param targetId [in] id of target in building
 * @return The position of specified target
 */

s_targetPosition NaoMarkPositionDatabase::getTargetPositionById(const char *building, int targetId) {
	s_targetPosition ret; // return structure
	stringstream selectQuery;
	string table = "targets";
	selectQuery << "SELECT * FROM " << table << " WHERE (id == " << targetId << ") AND (building == '" << building << "');";
	//qiLogInfo("sqlite3") << selectQuery.str().c_str() << std::endl;
	sqlite3_stmt *selectStatement;
	if ((sqlite3_prepare_v2(database, selectQuery.str().c_str(), -1, &selectStatement, 0)) == SQLITE_OK) {
		int cols = sqlite3_column_count(selectStatement);
		//cout << "returned columns: " << cols << endl;
		if (sqlite3_step(selectStatement) == SQLITE_ROW) {
			for (int i = 0; i < cols; i++) {
				if ((char*) sqlite3_column_text(selectStatement, i) != NULL) {
					fillOneItemInStructureTargetPosition(&ret, sqlite3_column_name(selectStatement, i), sqlite3_column_text(selectStatement, i));
				}
			}
			sqlite3_finalize(selectStatement);
			return ret;
		} else { // no result from db = unknown naoMark
			sqlite3_finalize(selectStatement);
			setStructureTargetPositionAsInvalid(&ret);
			ret.id = targetId;
			return ret;
		}
	} else {
		//	setStructureAsInvalid(&ret);
		throw "Wrong db name (or wrong select query)!\n";
		//		return ret;
	}
	return ret;
}

/**
 * Find all NaoMarks in specified room 
 * @param room [in] Id of room, where we want to get all NaoMarks which are there
 * @param nrOfReturnedNaoMarksPositions [out] Number of returned naoMarks in room (! OUTPUT PARAMETER !) 
 * @return Array of NaoMarks in room, the number of returned naoMarks is set in output parameter nrOfReturnedNaoMarksPositions 
 */
s_naoMarkPosition * NaoMarkPositionDatabase::getNaoMarksInRoom(const s_room * room, int * nrOfReturnedNaoMarksPositions) {
	int allocatedStructs = 0;
	s_naoMarkPosition* ret = new s_naoMarkPosition[allocatedStructs]; // return structure
	*nrOfReturnedNaoMarksPositions = 0;

	stringstream selectQuery;
	string buildingName(room->building);
	string roomName(room->name);
	selectQuery << "SELECT * FROM " << tableName << " WHERE "
			<< "(" << " building" << " == '" << buildingName.c_str() << "')" << "and"
			<< "(" << " floor" << " == " << room->building_floor << ")" << "and"
			<< "(" << " room" << " == '" << roomName.c_str() << "')" << "and"
			<< "(" << " roomType" << " == '" << createEnumOfRoomType(room->roomType) << "')" << ";";

	sqlite3_stmt *selectStatement;
	if ((sqlite3_prepare_v2(database, selectQuery.str().c_str(), -1, &selectStatement, 0)) == SQLITE_OK) {
		int cols = sqlite3_column_count(selectStatement); // number of returned columns from db

		if (sqlite3_step(selectStatement) != SQLITE_ROW) {
			sqlite3_finalize(selectStatement);
			throw "Unknown naoMark!\n";
			//			return ret;
		}

		int j = 0;
		do {
			if (j >= allocatedStructs) { // realloc array of structs (which will be returned in this method)
				ret = reallocArrayOfStruct(allocatedStructs, allocatedStructs * 2 + 1, ret);
				allocatedStructs = allocatedStructs * 2 + 1;
			}
			for (int i = 0; i < cols; i++) { // for all returned columns in one row
				if ((char*) sqlite3_column_text(selectStatement, i) != NULL) {
					fillOneItemInStructureNaoMarkPosition(&ret[j], sqlite3_column_name(selectStatement, i), sqlite3_column_text(selectStatement, i));
				}
			}
			j++;
		} while (sqlite3_step(selectStatement) != SQLITE_DONE); // for all returned rows
		*nrOfReturnedNaoMarksPositions = j;
		sqlite3_finalize(selectStatement);
		return ret;
	} else {
		//setStructureAsInvalid(ret);
		throw "Wrong db name (or wrong select query)!\n";
		//return ret;
	}
}

/**
 * Method for debugging only (print information about NaoMark)
 * @param mark [in] Array of NaoMarks to be printed
 * @param nrOfNaoMarksToPrint [in] Number of NaoMarks saved in previous parameter
 */
void NaoMarkPositionDatabase::printNaoMarkPosition(const s_naoMarkPosition* mark, int nrOfNaoMarksToPrint) {
	for (int i = 0; i < nrOfNaoMarksToPrint; i++) {
		cout << "|== NaoMark position(" << i + 1 << "/" << nrOfNaoMarksToPrint << ") ==" << endl;
		cout << "| NaoMark:" << endl;
		cout << "| 01) id: " << mark[i].mark.id << endl;
		cout << "| 02) height: " << mark[i].mark.heightFromGround << endl;
		cout << "| 03) radius: " << mark[i].mark.radius << endl;
		cout << "| 04) angle: " << mark[i].mark.angle << " grades" << endl;
		cout << "| Room:" << endl;
		cout << "| 05) building: ";
		printf("%s\n", mark[i].room.building);
		cout << "| 06) floor: " << mark[i].room.building_floor << endl;
		cout << "| 07) name: ";
		printf("%s\n", mark[i].room.name);
		cout << "| 08) type: " << createEnumOfRoomType(mark[i].room.roomType) << endl;
		cout << "| Common:" << endl;
		cout << "| 09) coord_X: " << mark[i].mark.coord_X << endl;
		cout << "| 10) coord_Y: " << mark[i].mark.coord_Y << endl;
		cout << "| 11) labeledObject: " << createEnumOfNaoMarkObject(mark[i].mark.naoMarkObject) << endl;
		cout << "| 12) labObjId: ";
		printf("%s\n", mark[i].naoMarkObjectId);
		cout << "| 13) info1: " << mark[i].info1 << endl;
		cout << "| 14) info2: " << mark[i].info2 << endl;
		cout << "| 15) info3: " << mark[i].info3 << endl;
		cout << "|===========================" << endl;
	}

}

/**
 * copy data between two different structures (from structure s_naoMarkPosition to structure s_room)
 * @param src [in] Source structure
 * @param dest [out] Destination structure
 */
void NaoMarkPositionDatabase::copyFromStructNaoMarkPositionToStructRoom(const s_naoMarkPosition* src, s_room* dest) {
	sprintf(dest->building, "%s", src->room.building);
	sprintf(dest->name, "%s", src->room.name);
	dest->building_floor = src->room.building_floor;
	dest->roomType = src->room.roomType;
}

/**
 * set default values for s_naoMarkPosition structure 
 * @param structure [out] Structure to be filled by values
 */
void NaoMarkPositionDatabase::setStructureNaoMarkAsInvalid(s_naoMarkPosition* structure) {
	structure->mark.coord_X = INT_MAX;
	structure->mark.coord_Y = INT_MAX;
	sprintf(structure->info1, "INVALID");
	sprintf(structure->info2, "INVALID");
	sprintf(structure->info3, "INVALID");
	sprintf(structure->naoMarkObjectId, "INVALID");
	sprintf(structure->room.building, "INVALID");
	sprintf(structure->room.name, "INVALID");
	structure->room.roomType = errorRoom;
	structure->room.building_floor = INT_MAX;
	structure->mark.naoMarkObject = errorObject;
	structure->mark.id = INT_MAX;
	structure->mark.radius = INT_MAX;
	structure->mark.angle = INT_MAX;
	structure->mark.heightFromGround = INT_MAX;

}

/**
 * Fill one single item in structure s_naoMarkPosition
 * @param structure [out] Structure which will be filled (only one item of this structure will be filled)
 * @param itemName [in] Name of structure item, which will be filled
 * @param content [in] Value which will be stored into one item (specified by 2nd parameter) of structure (specified by first parameter)
 */
void NaoMarkPositionDatabase::fillOneItemInStructureNaoMarkPosition(s_naoMarkPosition* structure, const char *itemName, const unsigned char* content) {
	//printf("%s, %s\n", itemName, content);
	if (strcmp(itemName, "building") == 0) {
		memcpy(structure->room.building, content, 16);
	} else if (strcmp(itemName, "coord_X") == 0) {
		structure->mark.coord_X = atoi((const char*) content);
	} else if (strcmp(itemName, "coord_Y") == 0) {
		structure->mark.coord_Y = atoi((const char*) content);
	} else if (strcmp(itemName, "floor") == 0) {
		structure->room.building_floor = atoi((const char*) content);
	} else if (strcmp(itemName, "naoMarkHeight") == 0) {
		structure->mark.heightFromGround = atoi((const char*) content);
	} else if (strcmp(itemName, "naoMarkRadius") == 0) {
		structure->mark.radius = atoi((const char*) content);
	} else if (strcmp(itemName, "naoMarkAngle") == 0) {
		structure->mark.angle = atoi((const char*) content);
	} else if (strcmp(itemName, "naoMarkObject") == 0) {
		structure->mark.naoMarkObject = getEnumOfNaoMarkObject((const char *) content);
	} else if (strcmp(itemName, "roomType") == 0) {
		structure->room.roomType = getEnumOfRoomType((const char *) content);
	} else if (strcmp(itemName, "info1") == 0) {
		memcpy(structure->info1, content, 16);
	} else if (strcmp(itemName, "info2") == 0) {
		memcpy(structure->info2, content, 16);
	} else if (strcmp(itemName, "info3") == 0) {
		memcpy(structure->info3, content, 16);
	} else if (strcmp(itemName, "naoMarkObjectId") == 0) {
		memcpy(structure->naoMarkObjectId, content, 16);
	} else if (strcmp(itemName, "room") == 0) {
		memcpy(structure->room.name, content, 16);
	} else if (strcmp(itemName, "naoMark_id") == 0) {
		structure->mark.id = atoi((const char*) content);
	} else {
		throw "unknown column in db\n";
	}
}

void NaoMarkPositionDatabase::setStructureTargetPositionAsInvalid(s_targetPosition* structure) {
	structure->x = INT_MAX;
	structure->y = INT_MAX;
	structure->radiusInDeg = INT_MAX;
	sprintf(structure->room.building, "INVALID");
	sprintf(structure->room.name, "INVALID");
	structure->room.roomType = errorRoom;
	structure->room.building_floor = INT_MAX;
}

/**
 * Fill one single item in structure s_naoMarkPosition
 * @param structure [out] Structure which will be filled (only one item of this structure will be filled)
 * @param itemName [in] Name of structure item, which will be filled
 * @param content [in] Value which will be stored into one item (specified by 2nd parameter) of structure (specified by first parameter)
 */
void NaoMarkPositionDatabase::fillOneItemInStructureTargetPosition(s_targetPosition* structure, const char *itemName, const unsigned char* content) {
	//printf("%s, %s\n", itemName, content);
	if (strcmp(itemName, "building") == 0) {
		memcpy(structure->room.building, content, 16);
	} else if (strcmp(itemName, "room") == 0) {
		memcpy(structure->room.name, content, 16);
	} else if (strcmp(itemName, "coord_X") == 0) {
		structure->x = atoi((const char*) content);
	} else if (strcmp(itemName, "coord_Y") == 0) {
		structure->y = atoi((const char*) content);
	} else if (strcmp(itemName, "floor") == 0) {
		structure->room.building_floor = atoi((const char*) content);
	} else if (strcmp(itemName, "positionOrientation") == 0) {
		structure->radiusInDeg = atoi((const char*) content);
	} else if (strcmp(itemName, "id") == 0) {
		structure->id = atoi((const char*) content);
	} else if (strcmp(itemName, "info1") == 0) {
		; //maybe TODO
	} else {
		throw "unknown column in db\n";
	}
}


/**
 * Convert string into enum e_roomType
 * @param obj [in] Object which will be converter into enum
 * @return e_roomType Enum which corresponds to the obj
 */
e_roomType NaoMarkPositionDatabase::getEnumOfRoomType(const char* obj) {
	if (strcmp(obj, "office") == 0) {
		return office;
	} else if (strcmp(obj, "classroom") == 0) {
		return classroom;
	} else if (strcmp(obj, "respirium") == 0) {
		return respirium;
	} else if (strcmp(obj, "corridor") == 0) {
		return corridor;
	} else if (strcmp(obj, "kitchen") == 0) {
		return kitchen;
	} else if (strcmp(obj, "storeroom") == 0) {
		return storeroom;
	} else if (strcmp(obj, "otherRoom") == 0) {
		return otherRoom;
	} else {
		return errorRoom;
	}
}

/**
 * Convert enum (e_roomType) into string
 * @param obj [in] Object which will be converter into string
 * @return string which corresponds to the obj
 */
string NaoMarkPositionDatabase::createEnumOfRoomType(e_roomType obj) {
	if (obj == classroom) {
		return "classroom";
	} else if (obj == corridor) {
		return "corridor";
	} else if (obj == errorRoom) {
		return "errorRoom";
	} else if (obj == kitchen) {
		return "kitchen";
	} else if (obj == office) {
		return "office";
	} else if (obj == otherRoom) {
		return "otherRoom";
	} else if (obj == respirium) {
		return "respirium";
	} else if (obj == storeroom) {
		return "storeroom";
	} else {
		return "errorRoom";
	}
}

/**
 * Convert enum (e_naoMarkObjects) into string
 * @param obj [in] Object which will be converter into string
 * @return string which corresponds to the obj
 */
string NaoMarkPositionDatabase::createEnumOfNaoMarkObject(e_naoMarkObjects obj) {
	if (obj == door) {
		return "door";
	} else if (obj == wall) {
		return "wall";
	} else if (obj == table) {
		return "table";
	} else if (obj == window) {
		return "window";
	} else if (obj == elevator) {
		return "elevator";
	} else if (obj == onFloor) {
		return "floor";
	} else if (obj == closet) {
		return "closet";
	} else if (obj == otherObject) {
		return "otherObject";
	} else {
		return "errorObject";
	}
}

/**
 * Convert string into enum e_naoMarkObjects
 * @param obj [in] Object which will be converter into enum
 * @return e_roomType which corresponds to the obj
 */
e_naoMarkObjects NaoMarkPositionDatabase::getEnumOfNaoMarkObject(const char* obj) {
	if (strcmp(obj, "door") == 0) {
		return door;
	} else if (strcmp(obj, "table") == 0) {
		return table;
	} else if (strcmp(obj, "wall") == 0) {
		return wall;
	} else if (strcmp(obj, "window") == 0) {
		return window;
	} else if (strcmp(obj, "elevator") == 0) {
		return elevator;
	} else if (strcmp(obj, "floor") == 0) {
		return onFloor;
	} else if (strcmp(obj, "closet") == 0) {
		return closet;
	} else if (strcmp(obj, "otherObject") == 0) {
		return otherObject;
	} else {
		return errorObject;
	}
}

/**
 * Reallocate array of structures s_naoMarkPosition (from oldSize size to newSize size) and copy the values from oldArray into (returned) newArray.
 * @param oldSize [in] Number of structures in old array
 * @param newSize [in] Number of structures in new array
 * @param oldArray [in,out] Old array of structures, this array is deleted after copying it´s values into newArray 
 * @return array of structures 
 */
s_naoMarkPosition * NaoMarkPositionDatabase::reallocArrayOfStruct(int oldSize, int newSize, s_naoMarkPosition* oldArray) {
	if (oldSize == newSize) {
		return oldArray;
	} else {
		s_naoMarkPosition * newArray = new s_naoMarkPosition[newSize];
		memcpy(newArray, oldArray, sizeof (s_naoMarkPosition) * min(oldSize, newSize));
		delete [] oldArray;
		oldArray = NULL;
		return newArray;
	}
}

/**
 * Destructor
 */
NaoMarkPositionDatabase::~NaoMarkPositionDatabase() {
	closeDatabase();
}

