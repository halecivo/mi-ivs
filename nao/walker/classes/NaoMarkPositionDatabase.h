/* 
 * File:   NaoMarkPositionDatabase.h
 * Author: hajekm13
 *
 * Created on 08 April 2014, 14:11
 */

#ifndef NAOMARKPOSITIONDATABASE_H
#define	NAOMARKPOSITIONDATABASE_H

#include <iostream> // string
#include "sqlite3.h"
#include "../structs.h"
class NaoMarkPositionDatabase {
public:
	NaoMarkPositionDatabase();
	s_naoMarkPosition getNaoMarkPositionById(int NaoMark);
	s_targetPosition getTargetPositionById(const char *building, int targetId);
	s_naoMarkPosition* getNaoMarksInRoom(const s_room *room, int *nrOfReturnedNaoMarksPositions);
	void printNaoMarkPosition(const s_naoMarkPosition *mark, int nrOfNaoMarksToPrint = 1); // should be just function, not method
	void copyFromStructNaoMarkPositionToStructRoom(const s_naoMarkPosition *src, s_room * dest); // should be just function, not method
	virtual ~NaoMarkPositionDatabase();
private:
	void fillOneItemInStructureNaoMarkPosition(s_naoMarkPosition * structure, const char* itemId, const unsigned char* content);
	void fillOneItemInStructureTargetPosition(s_targetPosition * structure, const char* itemId, const unsigned char* content);
	void setStructureNaoMarkAsInvalid(s_naoMarkPosition * structure);
	void setStructureTargetPositionAsInvalid(s_targetPosition * structure);
	bool openDatabase();
	bool closeDatabase();
	e_naoMarkObjects getEnumOfNaoMarkObject(const char *obj); // should be just function, not method
	std::string createEnumOfNaoMarkObject(e_naoMarkObjects obj); // should be just function, not method
	e_roomType getEnumOfRoomType(const char *obj); //should be just function, not method
	std::string createEnumOfRoomType(e_roomType obj); // should be just function, not method 
	s_naoMarkPosition *reallocArrayOfStruct(int oldSize, int newSize, s_naoMarkPosition * array);

	sqlite3 *database; /** pointer to database */

};

#endif	/* NAOMARKPOSITIONDATABASE_H */

