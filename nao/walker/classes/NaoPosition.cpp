/* 
 * File:   NaoPosition.cpp
 * Author: halecivo
 * 
 * Created on 2. duben 2014, 19:31
 */

#include "NaoPosition.h"
#include <alproxies/almemoryproxy.h>
#include <alproxies/almotionproxy.h>
#include <qi/log.hpp>
#include <math.h>
#include <vector>
#include <almath/tools/altransformhelpers.h>
#include <almath/tools/almathio.h>




#define MARK_SIZE 0.17
#define CAMERA "CameraTop"

using namespace AL;
using namespace Math;

NaoPosition::NaoPosition(ALValue naoMarkData, boost::shared_ptr<AL::ALBroker> pBroker) {
	int id = naoMarkData[1][0][1][0];
	double alpha = naoMarkData[1][0][0][1];
	double beta = naoMarkData[1][0][0][2];
	double sizeX = naoMarkData[1][0][0][3];
	double sizeY = naoMarkData[1][0][0][4];
	double heading = naoMarkData[1][0][0][5];

	double distance = MARK_SIZE / (2 * tan(sizeY / 2));

	ALMotionProxy motion = ALMotionProxy(pBroker);
	tts = ALTextToSpeechProxy(pBroker);

	std::vector<float> transform = motion.getTransform(CAMERA, 2, true);

	AL::Math::Transform robotToCamera = AL::Math::Transform(transform);
	//AL::Math::Transform rotationTrans = AL::Math::transformFrom3DRotation(0, beta, sizeX);
	AL::Math::Transform rotationTrans = AL::Math::transformFrom3DRotation(0, beta, alpha);
	AL::Math::Transform distanceTrans(distance, 0, 0);

	AL::Math::Transform robotToLandmark = robotToCamera * rotationTrans * distanceTrans;

	x = robotToLandmark.r1_c4;
	y = robotToLandmark.r2_c4;
	z = robotToLandmark.r3_c4;

	naoMarkId = id;
	naoMarkAngle = heading;
	naoMarkDistance = distance;

	qiLogInfo("NaoMark calculator: ") << "------------------------------------------------------------------" << std::endl;
	qiLogInfo("NaoMark calculator: ") << "znacka by udajne mela byt videt pod uhlem " << heading << " stupnu" << std::endl;
	qiLogInfo("NaoMark calculator: ") << "znacka byla mel byt posunuta o " << x << " metru v ose x" << std::endl;
	qiLogInfo("NaoMark calculator:") << "znacka byla mel byt posunuta o " << y << " metru v ose y" << std::endl;
	qiLogInfo("NaoMark calculator:") << "znacka byla mel byt posunuta o " << z << " metru v ose z" << std::endl;
	// calculate angle between Nao brest and naoMark	
	if (x == 0 && y > 0) {
		naoAngle = 0;
	} else if (x == 0 && y < 0) {
		naoAngle = 180;
	} else if (y == 0 && x > 0) {
		naoAngle = 90;
	} else if (y == 0 && x < 0) {
		naoAngle = 270;
	} else if (x == 0 && y == 0) {// invalid option, Nao is standing on naoMark, impossible to determine Nao orientation
		naoAngle = 0;
	} else {
		double quadrantAngleRadians = atan(abs(y / x));
		double quadrantAngleDegrees = (180 * quadrantAngleRadians / M_PI); // convert radians to degrees
		if (x > 0 && y > 0) { // quadrant ++
			naoAngle = 90 - quadrantAngleDegrees;
		} else if (x > 0 && y < 0) { // quadrant +-
			naoAngle = 90 + quadrantAngleDegrees;
		} else if (x < 0 && y < 0) { // quadrant --
			naoAngle = 270 - quadrantAngleDegrees;
		} else if (x < 0 && y > 0) { // quadrant -+
			naoAngle = 270 + quadrantAngleDegrees;
		}
	}
	qiLogInfo("NaoMark calculator:") << "Z hodnot x a y jsem vypocital uhel" << naoAngle << std::endl;
	qiLogInfo("NaoMark calculator: ") << "------------------------------------------------------------------" << std::endl;
	// updateNaoPosition(id,distance, heading);
	// sayNaoPosition();
}

double NaoPosition::getNaoAngle() {
	return naoAngle;
}

double NaoPosition::getNaoMarkDistance() {
	return naoMarkDistance;
}

double NaoPosition::getNaoMarkAngle() {
	return naoMarkAngle;
}

int NaoPosition::getNaoMarkId() {
	return naoMarkId;
}

double NaoPosition::getX() {
	return x;
}

double NaoPosition::getY() {
	return y;
}

double NaoPosition::getZ() {
	return z;
}

NaoPosition::~NaoPosition() {

}

