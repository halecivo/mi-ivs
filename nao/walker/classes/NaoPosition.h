/* 
 * File:   NaoPosition.h
 * Author: halecivo
 *
 * Created on 2. duben 2014, 19:31
 */

#ifndef NAOPOSITION_H
#define	NAOPOSITION_H
#include "NaoMarkPositionDatabase.h"
#include <boost/shared_ptr.hpp>
#include <alproxies/altexttospeechproxy.h>
#include <alcommon/almodule.h>
#include <cmath> // sin, cos
#include <string.h> // memcpy
#include "../structs.h"

class NaoPosition {
public:
	NaoPosition(AL::ALValue naoMarkData, boost::shared_ptr<AL::ALBroker> pBroker);
	NaoPosition(const NaoPosition& orig);
	virtual ~NaoPosition();
	double getNaoAngle();
	double getNaoMarkAngle();
	double getNaoMarkDistance();
	int getNaoMarkId();
	double getX();
	double getY();
	double getZ();
private:
	AL::ALTextToSpeechProxy tts;
	double x, y, z;
	double naoAngle;
	double naoMarkAngle;
	double naoMarkDistance;
	int naoMarkId;
};

#endif	/* NAOPOSITION_H */

