/* 
 * File:   structs.h
 * Author: Elephant
 *
 * Created on 14 May 2014, 13:23
 */

#ifndef STRUCTS_H
#define	STRUCTS_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif


/**
 * enum for different types of room, where NaoMarks can be places
 */
enum e_roomType {
	office, classroom, respirium, corridor, kitchen, storeroom, otherRoom, errorRoom
};

/**
 * enum for different types of objects in room, where NaoMarks can be places
 */
enum e_naoMarkObjects {
	door, wall, table, window, elevator, onFloor, closet, otherObject, errorObject
};

/**
 * struct for information about room
 */
struct s_room {
	char building[16]; // name of building (eg. TH:A for TH:A-1048)   http://www.fit.cvut.cz/en/student/novacek/ucebny 
	int building_floor; // floor number in building (just integers - no staircase) (eg. 10 for TH:A-1048)
	char name[16]; // name (number) of room (eg. 48 for TH:A-1048) or corridor1 - corridor5
	e_roomType roomType; // type of 'room' (office, classroom, respirium, corridor, kitchen, storeroom, other),(e.g. type classroom for A-1048)
};

/**
 * structure for information about NaoMark 
 */
struct s_naoMark {
	int id; // nrOfNaoMark
	int radius; // radius (it's the same as width or height) of naoMark (in cm)
	int heightFromGround; // height of the center of naoMark from the ground (in cm)
	int angle; // angle of NaoMark (in x and y axis system) in degrees. X+ = 0 degrees (East); Y- = 90 degrees (South); X- = 180 degrees (West);;Y+ = 270 degrees (North);
	int coord_X; //distance of naoMark in cm from reference point (of room) in X-axis
	int coord_Y; // distance of naoMark in cm from reference point (of room) in Y-axis
	e_naoMarkObjects naoMarkObject; // name of the object, which is labelled by naoMark

};

/**
 * structure for information about position of NaoMark (combines information about NaoMark, about Room and some other information)
 */
struct s_naoMarkPosition { // structure which is returned by method getNaoMarkPosition;
	s_room room;
	s_naoMark mark;
	char naoMarkObjectId[16]; //if naoMarkObject can be identified by id (door Id, or inventary number), this id is stored in naoMarkObjectId; 
	char info1[16]; // optional record
	char info2[16]; // optional record
	char info3[16]; // optional record
};


struct s_position {
	s_room room;
	int X_coord;
	int Y_coord;
	int orientation; // 0 grades = Y+; 90grades = X+; 180grades = Y-; 270grades = X-;
};

/**
 * structure for information about position of subtarget (combines information about subtarget and about room)
 * it's supposed nao Walks only direct in Y+ axis direction, without any theta angle.
 */
struct s_subTargetPosition { /* structure which is returned by method getNaoMarkPosition;*/
	int subId; /* id of subtarget, numerated from 0*/
	int numberOfTargets; /* id of subtarget*/
	s_room room;
	int distanceToGoInCm; /* distance to move in radiusInDeg rotation*/
	int x; /* x coord of next subtarget (redundant information, we can calculate this coord from distanceToGoInCm and radiusInDeg of previous subtarget)*/
	int y; /* y coord of next subtarget (redundant information, we can calculate this coord from distanceToGoInCm and radiusInDeg of previous subtarget)*/
	int radiusInDeg; /* Rotation in degrees (rotate BEFORE moving distanceToGoInCm centimeters!) */
};

/**
 * structure for information about position of target (combines information about target and about room)
 */
struct s_targetPosition { /* structure which is returned by method getTargetPosition;*/
	int id; /* id of target*/
	s_room room;
	int x; /* x coord*/
	int y; /* y coord*/
	int radiusInDeg; /* rotation in degrees*/
};


#endif	/* STRUCTS_H */

